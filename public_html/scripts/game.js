// hard-coded prototype level
var levelData = {
    background: "images/gameplay-background.jpg",
    fractalTech: 500,
    elements: [{
        type: "FractalBase",
        hp: 20,
        x: 100,
        y: 100
    }, {
        type: "TejmarBase",
        hp: 20,
        x: 700,
        y: 500
    }, {
        type: "TejmarShield",
        hp: 2,
        x: 600,
        y: 500
    }, {
        type: "Emitter",
        orientation: "up-left",
        hp: 1,
        x: 400,
        y: 400
    }]
};

// PixiJS Aliases
var Container = PIXI.Container,
        autoDetectRenderer = PIXI.autoDetectRenderer,
        loader = PIXI.loader,
        resources = PIXI.loader.resources,
        Sprite = PIXI.Sprite,
        TextureCache = PIXI.utils.TextureCache,
        Rectangle = PIXI.Rectangle,
        Graphics = PIXI.Graphics,
        DisplayObjectContainer = PIXI.DisplayObjectContainer,
        Text = PIXI.Text;

// rendering constants
var CANVAS_WIDTH = 1656;
var CANVAS_HEIGHT = 1056;
var CELL_LENGTH = 8;
var GRID_WIDTH = CANVAS_WIDTH / CELL_LENGTH;
var GRID_HEIGHT = CANVAS_HEIGHT / CELL_LENGTH;
var GRID_MARGIN_BOTTOM = GRID_HEIGHT - (160 / CELL_LENGTH);
var RENDERER_OPTIONS = {
    antialias: false,
    transparent: true,
    resolution: 1
};
var TEXT_STYLE =
        {fontFamily: "'Squada One', Serif",
        fontSize: "24px",
        fill: "white"
};

// element types
var BASE = 0;
var SHIELD = 1;
var EMITTER = 2;

// cell types
var EMPTY_CELL = 0;
var FRACTAL_CELL = 1;
var TEJMAR_CELL = 2;

// jet and other drag-and-drop types
var LIGHTWEIGHT_SPACE_SHIP_LEFT = 0;
var GLIDER_UP_LEFT = 1;
var LIGHTWEIGHT_SPACE_SHIP_UP = 2;
var GLIDER_UP_RIGHT = 3;
var LIGHTWEIGHT_SPACE_SHIP_RIGHT = 4;
var GLIDER_DOWN_RIGHT = 5;
var LIGHTWEIGHT_SPACE_SHIP_DOWN = 6;
var GLIDER_DOWN_LEFT = 7;
var SHIELD = 8;

// jet cell arrays
var jetCells = new Array(8);
jetCells[LIGHTWEIGHT_SPACE_SHIP_LEFT] =
        [1, 0, 4, 0, 0, 1, 0, 2, 4, 2, 0, 3, 1, 3, 2, 3, 3, 3];
jetCells[GLIDER_UP_LEFT] = [0, 0, 1, 0, 2, 0, 0, 1, 1, 2];
jetCells[LIGHTWEIGHT_SPACE_SHIP_UP] =
        [0, 0, 1, 0, 2, 0, 0, 1, 3, 1, 0, 2, 0, 3, 1, 4, 3, 4];
jetCells[GLIDER_UP_RIGHT] = [1, 0, 2, 0, 0, 1, 2, 1, 2, 2];
jetCells[LIGHTWEIGHT_SPACE_SHIP_RIGHT] =
        [1, 0, 2, 0, 3, 0, 4, 0, 0, 1, 4, 1, 4, 2, 0, 3, 3, 3];
jetCells[GLIDER_DOWN_RIGHT] = [1, 0, 2, 1, 0, 2, 1, 2, 2, 2];
jetCells[LIGHTWEIGHT_SPACE_SHIP_DOWN] =
        [0, 0, 2, 0, 3, 1, 3, 2, 0, 3, 3, 3, 1, 4, 2, 4, 3, 4];
jetCells[GLIDER_DOWN_LEFT] = [0, 0, 0, 1, 2, 1, 0, 2, 1, 2];

// color constants
FRACTAL_COLOR = 0x93DB3B;
TEJMAR_COLOR = 0x8B0003;
UI_COLOR = 0x393939;
MENU_BTN_COLOR = 0xFF5C17;
FRACTAL_TECH_COLOR = 0x0F0F0D;

// gameplay constants
EMISSION_RATE = .005;
HIT_DAMAGE = 1;
HIT_COOLDOWN_TIME = 250;
VALID_PLACEMENT_RADIUS = 250;
JET_COST = 50;
SHIELD_COST = 100;
SHIELD_ABSORB_AMOUNT = 50;

// textures for sprites
var gliderTexture;
var lightweightSpaceshipTexture;
var fractalBaseTexture;
var tejmarBaseTexture;
var fractalShieldTexture;
var tejmarShieldTexture;
var emitterTexture;

// PixiJS global variables
var renderer = autoDetectRenderer(CANVAS_WIDTH, CANVAS_HEIGHT,
        RENDERER_OPTIONS);
renderer.view.style.backgroundImage = "url(" + levelData.background + ")";
renderer.view.style.backgroundSize = "100% 100%";
var stage = new Container();
var fractalGraphics = new Graphics();
var tejmarGraphics = new Graphics();
var uiGraphics = new Graphics();
stage.addChild(fractalGraphics);
stage.addChild(tejmarGraphics);
stage.addChild(uiGraphics);
var bump = new Bump(PIXI);
var tink = new Tink(PIXI, renderer.view);
var pointer = tink.makePointer();
var menuCoord = {left: 800, top: 872, right: 896, bottom: 920};
var fractalTechText;

// game variables
var renderGrid = [];
var updateGrid = [];
var fractalTech = 0;
var numFractalBases = 0;
var numTejmarBases = 0;
var fractalElemArray = [];
var tejmarElemArray = [];
var emitterArray = [];
var hitQueue = [];
var stepTimer = null;

// draggable sprites for fractal jets
var draggables = new Array(9);
var selectedDraggable = null;
var placementFeedbackTimer = null;

var Element = function (elem, tejmar) {
    this.tejmar = tejmar;
    this.hp = elem.hp;
    this.hitCooldown = false;
    
    if (this.tejmar) {
        tejmarElemArray.push(this);
    } else {
        fractalElemArray.push(this);
    }
    
    this.hit = function(damage) {
        var elem = this;
        if (elem.hitCooldown === false) {
            elem.hitCooldown = true;
            setTimeout(function() { elem.hitCooldown = false; }, HIT_COOLDOWN_TIME);
            elem.hp -= damage;
            if (!this.tejmar && this.type === SHIELD) {
                adjustFractalTech(SHIELD_ABSORB_AMOUNT);
            }
            if (elem.hp <= 0) {
                elem.destroy();
            }
        }
    };
    
    this.destroy = function() {
        this.sprite.visible = false;
        var arr;
        if (this.tejmar) {
            if (this.type === BASE) numTejmarBases--;
            arr = tejmarElemArray;
        } else {
            if (this.type === BASE) numFractalBases--;
            arr = fractalElemArray;
        }
        
        var index = arr.indexOf(this);
        arr.splice(index, 1);
        
        if (this.type === EMITTER) {
            var eIndex = emitterArray.indexOf(this);
            emitterArray.splice(eIndex, 1);
        }
    
        if (numFractalBases === 0) {
            // lose
        } else if (numTejmarBases === 0) {
            // win
        }
    };
};

function Base(b, tejmar) {
    Element.call(this, b, tejmar);
    this.type = BASE;
    
    if (tejmar) {
        numTejmarBases++;
        this.sprite = new Sprite(tejmarBaseTexture);
    } else {
        numFractalBases++;
        this.sprite =  new Sprite(fractalBaseTexture);
    }
    this.sprite.x = b.x;
    this.sprite.y = b.y;
    stage.addChild(this.sprite);
}

function Shield(s, tejmar) {
    Element.call(this, s, tejmar);
    this.type = SHIELD;
    
    if (tejmar) {
        this.sprite = new Sprite(tejmarShieldTexture);
    } else {
        this.sprite = new Sprite(fractalShieldTexture);
    }
    
    this.sprite.x = s.x;
    this.sprite.y = s.y;
    stage.addChild(this.sprite);
}

function Emitter(e) {
    Element.call(this, e, true);
    this.type = EMITTER;
    this.sprite = new Sprite(emitterTexture);
    this.sprite.x = e.x;
    this.sprite.y = e.y;
    switch (e.orientation) {
        case "left":
            this.cells = jetCells[LIGHTWEIGHT_SPACE_SHIP_LEFT];
            this.emitX = this.sprite.x;
            this.emitY = this.sprite.y;
            break;
        case "up-left":
            this.sprite.rotation = Math.PI / 4;
            this.cells = jetCells[GLIDER_UP_LEFT];
            this.emitX = this.sprite.x - 30;
            this.emitY = this.sprite.y;
            break;
        case "up":
            this.sprite.rotation = Math.PI / 2;
            this.cells = jetCells[LIGHTWEIGHT_SPACE_SHIP_UP];
            this.emitX = this.sprite.x - 35;
            this.emitY = this.sprite.y - 30;
            break;
        case "up-right":
            this.sprite.rotation = 3 * Math.PI / 4;
            this.cells = jetCells[GLIDER_UP_RIGHT];
            this.emitX = this.sprite.x - 25;
            this.emitY = this.sprite.y - 30;
            break;
        case "right":
            this.sprite.rotation = Math.PI;
            this.cells = jetCells[LIGHTWEIGHT_SPACE_SHIP_RIGHT];
            this.emitX = this.sprite.x;
            this.emitY = this.sprite.y - 35;
            break;
        case "down-right": // done
            this.sprite.rotation = Math.PI * 5 / 4;
            this.cells = jetCells[GLIDER_DOWN_RIGHT];
            this.emitX = this.sprite.x + 5;
            this.emitY = this.sprite.y - 40;
            break;
        case "down":
            this.sprite.rotation = Math.PI * 3 / 2;
            this.cells = jetCells[LIGHTWEIGHT_SPACE_SHIP_DOWN];
            this.emitX = this.sprite.x + 10;
            this.emitY = this.sprite.y - 5;
            break;
        case "down-left": //working
            this.sprite.rotation = Math.PI * 7 / 4;
            this.cells = jetCells[GLIDER_DOWN_LEFT];
            this.emitX = this.sprite.x;
            this.emitY = this.sprite.y + 20;
            break;
        default: throw "invalid orientation";
    }
}

function adjustFractalTech(amt) {
    fractalTech += amt;
    fractalTechText.text = "Fractal Tech: " + fractalTech;
}

function load() {
    loader
            .add("images/Glider.png")
            .add("images/LightweightSpaceship.png")
            .add("images/base.bmp")
            .add("images/Doomday.bmp")
            .add("images/Teleport.bmp")
            .add("images/Smasher.bmp")
            .add("images/Cargo1.bmp")
            .load(setup);          
}

function setup() {
    
    // initialize grids to all dead cells
    for (var row = 0; row < GRID_HEIGHT; row++) {
        for (var col = 0; col < GRID_WIDTH; col++) {
            setGridCell(renderGrid, row, col, EMPTY_CELL); 
            setGridCell(updateGrid, row, col, EMPTY_CELL);
        }
    }
    
    // initalize all the textures we will need
    gliderTexture = TextureCache["images/Glider.png"];
    lightweightSpaceshipTexture = TextureCache["images/LightweightSpaceship.png"];
    fractalBaseTexture = getTexture("images/base.bmp", 22, 15, 100, 85);
    tejmarBaseTexture = getTexture("images/Doomday.bmp", 0, 0, 48, 48);
    fractalShieldTexture = getTexture("images/Teleport.bmp", 104, 9, 32, 30);
    tejmarShieldTexture = getTexture("images/Smasher.bmp", 5, 5, 39, 39);
    emitterTexture = getTexture("images/Cargo1.bmp", 0, 70, 70, 50);
    
    fractalTech = levelData.fractalTech;
    renderUI();
    
    for (var i = 0; i < draggables.length; i++) {
        renderDraggable(i);
    }
    
    // initalize event handlers
    initEventHandlers();
    
    // add each of the elements listed in the levelData object
    levelData.elements.forEach(addElement);
    
    // add canvas to page
    document.body.appendChild(renderer.view);
    
    // start game loop
    gameLoop();
    stepTimer = setInterval(step, 1000 / 30);
}

function renderUI() {
    
    // dimensions
    var barHeight = 160;
    var menuButtonWidth = 96;
    var menuButtonHeight = 48;
    var techRectWidth = 224;
    var techRectHeight = 48;
    
    // save menu coordinates for detecting click
    menuCoord.left = CANVAS_WIDTH / 2 - (menuButtonWidth + techRectWidth) / 2;
    menuCoord.right = menuCoord.left + menuButtonWidth;
    menuCoord.top = CANVAS_HEIGHT - barHeight - menuButtonHeight;
    menuCoord.bottom = CANVAS_HEIGHT - barHeight;
    
    //  draw bottom bar for holding jets and other placeable resources
    uiGraphics.beginFill(UI_COLOR);
    uiGraphics.drawRect(0, CANVAS_HEIGHT - barHeight, CANVAS_WIDTH, barHeight);
    uiGraphics.endFill();
    
    // draw menu button
    uiGraphics.beginFill(MENU_BTN_COLOR);
    uiGraphics.drawRect(menuCoord.left, menuCoord.top,
            menuButtonWidth, menuButtonHeight);
    uiGraphics.endFill();
    
    // draw rectangle for displaying available fractal tech
    uiGraphics.beginFill(FRACTAL_TECH_COLOR);
    uiGraphics.drawRect(menuCoord.right, menuCoord.top,
            techRectWidth, techRectHeight);
    uiGraphics.endFill();
    
    // create ui text
    var menuText = new Text("Menu", TEXT_STYLE);
    menuText.position.set(menuCoord.left + 20, menuCoord.top + 10);
    fractalTechText = new Text("Fractal Tech: "
            + fractalTech, TEXT_STYLE);
    fractalTechText.position.set(menuCoord.right + 20, menuCoord.top + 10);
    stage.addChild(menuText);
    stage.addChild(fractalTechText);
}

function isIn(x, y, box) {
    return x >= box.left && x <= box.right && y >= box.top && y <= box.bottom;
}

function onMenuClick() {
    console.log("Menu clicked");
}

function renderDraggable(i) {
    var d;
    switch (i) {
        case LIGHTWEIGHT_SPACE_SHIP_LEFT:
            d = new Sprite(lightweightSpaceshipTexture);
            break;
        case GLIDER_UP_LEFT:
            d = new Sprite(gliderTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = Math.PI;
            break;
        case LIGHTWEIGHT_SPACE_SHIP_UP:
            d = new Sprite(lightweightSpaceshipTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = Math.PI / 2;
            break;
        case GLIDER_UP_RIGHT:
            d = new Sprite(gliderTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = 3 * Math.PI / 2;
            break;
        case LIGHTWEIGHT_SPACE_SHIP_RIGHT:
            d = new Sprite(lightweightSpaceshipTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = Math.PI;
            break;
        case GLIDER_DOWN_RIGHT:
            d = new Sprite(gliderTexture);
            break;
        case LIGHTWEIGHT_SPACE_SHIP_DOWN:
            d = new Sprite(lightweightSpaceshipTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = 3 * Math.PI / 2;
            break;
        case GLIDER_DOWN_LEFT:
            d = new Sprite(gliderTexture);
            d.anchor.x = 0.5;
            d.anchor.y = 0.5;
            d.rotation = Math.PI / 2;
            break;
        case SHIELD:
            d = new Sprite(fractalShieldTexture);
            break;
        default:
            throw "invalid draggable id";
    }
    
    if (i === SHIELD) d.cost = SHIELD_COST;
    else d.cost = JET_COST;
    d.i = i;
    d.x = 128 * i + 128;
    d.y = CANVAS_HEIGHT - 80;
    d.cells = jetCells[i];
    tink.makeDraggable(d);
    draggables[i] = d;
    stage.addChild(d);
}

function initEventHandlers() {
    pointer.press = function() {
        if (isIn(pointer.x, pointer.y, menuCoord)) {
            onMenuClick();
        }
        
        for (var i = 0; i < draggables.length; i++) {
            if (pointer.hitTestSprite(draggables[i])) {
                selectedDraggable = draggables[i];
                placementFeedbackTimer = setInterval(queryPlacement, 100);
                
            }
        }
    };
    
    pointer.release = function() {
        if (selectedDraggable) {
            clearInterval(placementFeedbackTimer);
            selectedDraggable.visible = false;
            var i = selectedDraggable.i;
     
            if (isValidPlacement()) {
                adjustFractalTech(-selectedDraggable.cost);
                
                if (i === 8) {
                    new Shield({
                        hp: 3,
                        x: selectedDraggable.x,
                        y: selectedDraggable.y
                    }, false);
                } else {
                    var cells = selectedDraggable.cells;
                    var x = selectedDraggable.x;
                    var y = selectedDraggable.y;
                    var col = Math.floor(x / CELL_LENGTH);
                    var row = Math.floor(y / CELL_LENGTH);
                    insertCells(cells, row, col, FRACTAL_CELL);
                }
            }
            
            selectedDraggable = null;
            renderDraggable(i);
        }
    };
}

function queryPlacement() {
    if (isValidPlacement()) {
        selectedDraggable.alpha = 1;
    } else {
        selectedDraggable.alpha = 0.2;
    }
}

function isValidPlacement() {
    if (selectedDraggable.cost > fractalTech) return false;
    
    var x1 = pointer.x;
    var y1 = pointer.y;
    var x2, y2;
    
    for (var i = 0; i < fractalElemArray.length; i++) {
        var b = fractalElemArray[i].sprite;
        x2 = b.x + b.width / 2;
        y2 = b.y + b.height / 2;
        
        if (distance(x1, y1, x2, y2) <= VALID_PLACEMENT_RADIUS) {
            return true;
        }
    }
    
    return false;
}

function distance(x1, y1, x2, y2) {
    return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

function gameLoop() {

  // loop this function at 60 frames per second
  requestAnimationFrame(gameLoop);
  
  // update Tink
  tink.update();
 
  // render the cells
  fractalGraphics.clear();
  tejmarGraphics.clear();
  renderCells();

  // render the stage
  renderer.render(stage);
}

function step() {
    processHitQueue();
    stepCells();
    emitterArray.forEach(queryEmitters);
}

function processHitQueue() {
    while (hitQueue.length > 0) {
        hitElement = hitQueue.shift();
        hitElement.hit(HIT_DAMAGE);
    }
}

function stepCells() {
    for (var row = 0; row < GRID_HEIGHT; row++) {
        for (var col = 0; col < GRID_WIDTH; col++) {
            var cellType = getGridCell(updateGrid, row, col);
            var numNeighbors = calcNeighbors(row, col, cellType);
            
            if (cellType === EMPTY_CELL) {
                if (numNeighbors.neighbors === 3) {
                    setGridCell(renderGrid, row, col, numNeighbors.cellType);
                } else {
                    setGridCell(renderGrid, row, col, EMPTY_CELL);
                }
            } else if (detectCollision(row, col, cellType) 
                    || numNeighbors === -1) {
                eraseCells(row, col, 5);
            } else if (numNeighbors < 2 || numNeighbors > 3) {
                setGridCell(renderGrid, row, col, EMPTY_CELL);
            } else {
                setGridCell(renderGrid, row, col, cellType);
            }
        }
    }
    
    var temp = updateGrid;
    updateGrid = renderGrid;
    renderGrid = temp;
}

function calcNeighbors(row, col, cellType) {
    var fractalCells = 0, tejmarCells = 0;
    
    if (getGridCell(updateGrid, row, col -1) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row - 1, col - 1) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row - 1, col) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row - 1, col + 1) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row, col + 1) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row + 1, col + 1) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row + 1, col) === FRACTAL_CELL) fractalCells++;
    if (getGridCell(updateGrid, row + 1, col - 1) === FRACTAL_CELL) fractalCells++;
    
    if (getGridCell(updateGrid, row, col -1) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row - 1, col - 1) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row - 1, col) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row - 1, col + 1) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row, col + 1) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row + 1, col + 1) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row + 1, col) === TEJMAR_CELL) tejmarCells++;
    if (getGridCell(updateGrid, row + 1, col - 1) === TEJMAR_CELL) tejmarCells++;
    
    if (cellType === EMPTY_CELL) {
        if (fractalCells === 0 && tejmarCells === 0)
            return {cellType: FRACTAL_CELL, neighbors: 0};
        else if (fractalCells > 0 && tejmarCells === 0)
            return {cellType: FRACTAL_CELL, neighbors: fractalCells};
        else if (tejmarCells > 0 && fractalCells === 0)
            return {cellType: TEJMAR_CELL, neighbors: tejmarCells};
        else
            return {cellType: FRACTAL_CELL, neighbors: 0};  
    } else if (cellType === FRACTAL_CELL) {
        return (tejmarCells > 0) ? -1 : fractalCells;
    } else {
        return (fractalCells > 0) ? -1 : tejmarCells;
    }
}

function eraseCells(row, col, radius) {
    for (var i = row - radius; i < row + radius ; i++) {
        for (var j = col - radius; j < col + radius; j++) {
            setGridCell(renderGrid, i, j, EMPTY_CELL); 
            setGridCell(updateGrid, i, j, EMPTY_CELL);
        }
    }
}

function queryEmitters(e) {
    if (e.cooldown > 0) {
        e.cooldown--;
    } else if (EMISSION_RATE >= Math.random()) {
        e.cooldown = 30;
        var col = Math.floor(e.emitX / CELL_LENGTH);
        var row = Math.floor(e.emitY / CELL_LENGTH);
        insertCells(e.cells, row, col, TEJMAR_CELL);
    }
}

function insertCells(cells, row, col, type) {
    for (var i = 0; i < cells.length; i += 2) {
            var y = col + cells[i];
            var x = row + cells[i+1];
            setGridCell(renderGrid, x, y, type);
            setGridCell(updateGrid, x, y, type);
    }
}

function detectCollision(row, col, cellType) {
    var pt = {x: col * CELL_LENGTH, y: row * CELL_LENGTH};
    var arr = (cellType === FRACTAL_CELL) ? tejmarElemArray : fractalElemArray;
    
    for (var i = 0; i < arr.length; i++) {
        var collision = bump.hit(pt, arr[i].sprite);
        if (collision) {
            hitQueue.push(arr[i]);
            return true;
        }
    }
    
    return false;
}

/*
 * Accessor method for getting the cell value in the grid at
 * location (row, col).
 */
function getGridCell(grid, row, col) {
    var index = (row * GRID_WIDTH) + col;
    return grid[index];
}

/*
 * Mutator method for setting the cell value in the grid at
 * location (row, col).
 */
function setGridCell(grid, row, col, value) {
    var index = (row * GRID_WIDTH) + col;
    grid[index] = value;
}

function getTexture(url, x, y, width, height) {
    var texture = TextureCache[url];
    texture.frame = new Rectangle(x, y, width, height);
    return texture;
}

function addElement(e) {
    switch (e.type) {
        case "FractalBase":
            new Base(e, false); break;
        case "TejmarBase":
            new Base(e, true); break;
        case "TejmarShield":
            new Shield(e, true); break;
        case "Emitter":
            var emitter = new Emitter(e);
            stage.addChild(emitter.sprite);
            emitterArray.push(emitter);
            break;
        default:
            throw "invalid element type";
    }
}

function renderCells() {
    
    // set the proper rendering colors
    fractalGraphics.beginFill(FRACTAL_COLOR);
    tejmarGraphics.beginFill(TEJMAR_COLOR);
    
    // render the cells in the grid
    for (var row = 0; row < GRID_HEIGHT; row++) {
        for (var col = 0; col < GRID_WIDTH; col++) {
            var cell = getGridCell(renderGrid, row, col);
            if (cell === FRACTAL_CELL) {
                var x = col * CELL_LENGTH;
                var y = row * CELL_LENGTH;
                fractalGraphics.drawRect(x, y, CELL_LENGTH, CELL_LENGTH);
            } else if (cell === TEJMAR_CELL) {
                var x = col * CELL_LENGTH;
                var y = row * CELL_LENGTH;
                tejmarGraphics.drawRect(x, y, CELL_LENGTH, CELL_LENGTH);
            }
        }
    }
    
    fractalGraphics.endFill();
    tejmarGraphics.endFill();
}

window.onload = load;
